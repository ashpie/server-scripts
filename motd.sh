#!/bin/bash

### Time and date ###
echo -e "$(date)"
echo -e


### Hostname ###
echo -e "$(/usr/bin/env figlet -w $(tput cols) "$(hostname)" | /usr/bin/env lolcat -f -F 0.05 -S $(echo $((0x$(hostname | md5sum | cut -c 1-10))) | cut -c 2-5))"

echo -e


### System info ###
echo -e "$(cat /etc/*release | grep PRETTY_NAME | cut -d = -f 2- | sed 's/\"//g') | Kernel: $(uname -r)
Uptime: $(uptime -p | sed 's/up //')"


### Services ###
# set column width
COLUMNS=1
# colors
green="\e[1;32m"
red="\e[1;31m"
undim="\e[0m"

services=($(cat /.motd.services))
# sort services
IFS=$'\n' services=($(sort <<<"${services[*]}"))
unset IFS

service_status=()
# get status of all services
for service in "${services[@]}"; do
    service_status+=($(systemctl is-active "$service"))
done

out=""
for i in ${!services[@]}; do
    # color green if service is active, else red
    if [[ "${service_status[$i]}" == "active" ]]; then
        out+="${services[$i]}:,${green}${service_status[$i]}${undim},"
    else
        out+="${services[$i]}:,${red}${service_status[$i]}${undim},"
    fi
    # insert \n every $COLUMNS column
    if [ $((($i+1) % $COLUMNS)) -eq 0 ]; then
        out+="\n"
    fi
done
out+="\n"

printf "\nServices:\n"
printf "$out" | column -ts $',' | sed -e 's/^/  /'


### Disk space ###
# config
max_usage=90
bar_width=50
# colors
white="\e[39m"
green="\e[1;32m"
red="\e[1;31m"
dim="\e[2m"
undim="\e[0m"

# disk usage: ignore zfs, squashfs & tmpfs
DISKS=$(df -H -x squashfs -x tmpfs -x devtmpfs -x zfs --output=target,pcent,size | tail -n+2)

if test $(command -v zpool); then
	DISKS+="\n"
	DISKS+=$(zpool list -o name,capacity,size | tail -n+2)
fi
printf "\nDisk usage:\n"

while read line; do
	if [ -z "$line" ]; then
		continuetest 
	fi
    # get disk usage
    usage=$(echo "$line" | awk '{print $2}' | sed 's/%//')
    used_width=$((($usage*$bar_width)/100))
    # color is green if usage < max_usage, else red
    if [ "${usage}" -ge "${max_usage}" ]; then
        color=$red
    else
        color=$green
    fi
    # print green/red bar until used_width
    bar="[${color}"
    for ((i=0; i<$used_width; i++)); do
        bar+="="
    done
    # print dimmmed bar until end
    bar+="${white}${dim}"
    for ((i=$used_width; i<$bar_width; i++)); do
        bar+="="
    done
    bar+="${undim}]"
    # print usage line & bar
    echo "${line}" | awk '{ printf("%-31s%+3s used out of %+4s\n", $1, $2, $3); }' | sed -e 's/^/  /'
    echo -e "${bar}" | sed -e 's/^/  /'
done <<< $(echo -e "$DISKS")


### Welcome message ###
LAST_LOGIN=$(last -w | grep arisu | head -2 | tail -1)
echo -e
echo -e "Welcome to $(hostname), $(whoami)!
Last login: $(echo "$LAST_LOGIN" | awk '{for(i=4;i<=NF;i++) print $i}' | paste -sd ' ') from $(echo "$LAST_LOGIN" | awk '{print $3}')"

### ZFS available space warning ###
if test $(command -v zfs); then
	POOLS=$(zfs list -o name,available -p | tail -n+2)
	while read line; do
		name=$(echo "$line" | awk '{print $1}')
		avail=$(echo "$line" | awk '{print $2}')

		if [ $avail -le 150000000000 ]; then
			echo -e "${red}Warning! ${white}$name ${undim}has less than 150GB"
		fi
	done <<< $(echo -e "$POOLS")
fi