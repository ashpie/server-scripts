#!/bin/sh
# ZFS Scrub script

POOLS=($(cat ~/.zfs_scrub_pools))
POOLS_STR=""

for pool in "${POOLS[@]}"; do
	echo "Scrub $pool"
	zpool scrub $pool
	POOLS_STR="$POOLS_STR$pool "
done

OUTPUT=$(zpool status)

echo "Sending mail..."
echo -e "Started scrub procedure for $POOLS_STR\n\n$OUTPUT" | $(dirname $0)/mail.sh "ZFS Scrub - $(hostname)"
echo "Done"
