#!/bin/sh

UPDATES=$(arch-audit --upgradable --show-testing | sed "{s/\n/<br>/g}")

if test "$1" != "true"
then
    UPDATES=$(echo -e "$UPDATES" | grep -v "testing repos\!")
fi

echo -en "$UPDATES"

if [ ! -z "$UPDATES" ]; then
	HOST=$(hostname)
	echo -e "The following security updates are available for $HOST:\n\n$UPDATES\n\nIt is highly recommended that you perform these updates now" | $(dirname $0)/mail.sh "Security updates are available on $HOST"
	echo "Mail sent"
fi

