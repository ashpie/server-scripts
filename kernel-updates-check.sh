#!/bin/bash

echo "Checking for kernel updates..."
UPDATES=$(checkupdates --download)
echo -e "$UPDATES"

if [ ! -z "$(echo -en $UPDATES | grep linux)" ]; then
	echo "Updates available!"
	echo -e "Updates are available:\n\n$UPDATES" | $(dirname $0)/mail.sh "Updates are available on $(hostname)"
fi
