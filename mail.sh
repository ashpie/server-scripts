#!/bin/bash
MAILTO=$(cat "$HOME/.admin_mail")

if [ -f "$HOME/.host_mail" ]; then
	MAILFROM=$(cat "$HOME/.host_mail")
fi

SUBJECT=$1
cat | mailx -s "$SUBJECT" -r "$MAILFROM" "$MAILTO"
