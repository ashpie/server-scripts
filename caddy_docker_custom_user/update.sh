#!/bin/sh

if [ $# -ne 2 ]; then
        echo "Usage: <script> <uid> <gid>"
	exit
fi

./create.sh $1 $2

if [ $? -ne 0 ]; then
	echo "Couldn't build caddy-custom image"
	exit
fi

HOST=$(hostname)
RUN_SCRIPT="run_$HOST.sh"

if [ ! -f $RUN_SCRIPT ]; then
	echo "$RUN_SCRIPT doesn't exist"
	exit
fi

docker container stop caddy && docker container rm caddy

if [ $? -ne 0 ]; then
	echo "Couldn't stop and rm caddy container"
	exit
fi

./$RUN_SCRIPT caddy-custom
