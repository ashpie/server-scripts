#!/bin/sh

if [ $# -eq 0 ]
then
        echo "Usage: <script> <uid> <gid>"
else
	puid=$1
	pgid=$2

	cd builder
	docker build --tag caddy-builder .
	cd ..

	docker build --build-arg PUID=$puid --build-arg PGID=$pgid --tag caddy-custom .
fi
