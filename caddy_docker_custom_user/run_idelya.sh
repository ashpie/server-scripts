#!/bin/sh

if [ $# -eq 0 ]
then
	echo "Usage: <script> <container_id>"
else
CONTAINER_ID=$1
docker run --name caddy -d --restart always \
	-v /home/website/caddy/caddy.conf:/etc/Caddyfile \
	-v /home/website/.caddy:/root/.caddy \
	-v /home/arisu/rtmp:/home/arisu/rtmp \
	-v /home/website:/home/website \
	-p 80:80 -p 443:443 -p 2015:2015 -p 4889:4889 -p 4887:4887 \
	-e ACME_AGREE=true \
	$CONTAINER_ID
fi
