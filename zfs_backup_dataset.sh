#!/bin/sh

DATASET=$1
if test -z "$DATASET"; then
	echo "Missing dataset argument"
	exit 1
fi

TARGET=$2
if test -z "$TARGET"; then
	echo "Missing target"
	exit 1
fi

TARGET_PATH=$3
if test -z "$TARGET_PATH"; then
	echo "Missing target path"
	exit 1
fi

### New snapshot ###
NAME=$(date +%Y-%m-%d_%H-%M-%S)
SNAPSHOT="$DATASET@$NAME"

echo "New snapshot name is: $NAME"
echo "New snapshot is: $SNAPSHOT"


### Default last snapshot ###
DEFAULT_LAST_SNAPSHOT=$(zfs list -t snapshot -o name -s creation -H $DATASET | tail -n 1)
DEFAULT_LAST_NAME=$(echo -e $DEFAULT_LAST_SNAPSHOT | rev | cut -d"@" -f1 | rev)
echo "Default last snapshot is: $DEFAULT_LAST_SNAPSHOT"
echo "Default last snapshot name is: $DEFAULT_LAST_NAME"


### Tracking folder ###
tracking_folder="$HOME/.local/share/zfs_backup"
if test ! -d $tracking_folder; then
	echo "Creating backup tracking files directory"
	mkdir -p $tracking_folder
fi


### Last snapshot ###
LAST_SNAPSHOT=$DEFAULT_LAST_SNAPSHOT

last_snapshot_file="$tracking_folder/last_snapshot_"$(echo -e $DATASET | sed -r "s/\//_/g")
if test -f $last_snapshot_file; then
	LAST_SNAPSHOT=$(cat $last_snapshot_file)
	echo "Found $last_snapshot_file"
else
	echo "No $last_snapshot_file, resolving to default"
fi

LAST_SNAPSHOT_NAME=$(echo -e $LAST_SNAPSHOT | rev | cut -d"@" -f1 | rev)

echo "Last snapshot is: $LAST_SNAPSHOT"
echo "Last snapshot name is: $LAST_SNAPSHOT_NAME"

echo "Starting backup with args $DATASET $NAME $LAST_SNAPSHOT_NAME"
$HOME/scripts/zfs_backup.sh $DATASET $NAME $LAST_SNAPSHOT_NAME $TARGET $TARGET_PATH || exit 1

echo "Saving new snapshot in tracking folder..."
echo -e $SNAPSHOT > $last_snapshot_file

echo "Destroying last snapshot..."
zfs destroy $LAST_SNAPSHOT || exit 1

echo "Backup finished."
